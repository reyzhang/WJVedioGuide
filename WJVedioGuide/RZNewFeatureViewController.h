//
//  RZNewFeatureViewController.h
//
//
//  Created by reyzhang on 2018/5/29.
//  Copyright © 2018年 . All rights reserved.
//

#import "ViewController.h"



@interface RZNewFeatureViewController : ViewController

@property (nonatomic,strong) NSString *movie_url;

/*!
 根据本地视频文件来实例化
 */
- (instancetype)initWithMovieFile:(NSString *)movie;


@end
